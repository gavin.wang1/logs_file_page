import unittest
from unittest import mock
from tornado.web import Application
from tornado.httpserver import HTTPRequest
from logs_file_page.log_show import LogFileHandler, LogShowHandler


class MyTestCase(unittest.TestCase):

    def test_LogFileHandler_find_files(self):
        container = LogFileHandler.find_files(path="./", file_subtitle=".py")
        for unit in ['test_handler.py', 'log_show.py', '__init__.py']:
            self.assertIn(unit, container)

    def test_LogShowHandler_re_body(self):
        res = LogShowHandler.re_body(log_record_arr=["第1句話", "第2句話", "第3句話", "第4句話"], lines=2, br=2,word_filter=None)
        self.assertEqual(res,"第3句話<br><br>第4句話")

if __name__ == '__main__':
    unittest.main()
