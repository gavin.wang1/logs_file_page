from typing import Optional, Awaitable

import tornado.web
import os



class LogFileHandler(tornado.web.RequestHandler):
    @staticmethod
    def find_files(path, file_subtitle)->list:
        return [file for file in os.listdir(path) if file.endswith(file_subtitle)]

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self):
        try:
            path = self.get_argument("path", "./resource")
            file_subtitle = self.get_argument("subtitle", ".log")
            assert path.startswith("./"), "not support path"
            assert file_subtitle in [".txt",".log"], "not support file type"
            self.write({
                "path":path,
                "subtitle":file_subtitle,
                "files": self.find_files(path=path,file_subtitle=file_subtitle)
            })
        except AssertionError as e:
            self.set_status(status_code=400,reason=str(e))
            self.write_error(status_code=400)


class LogShowHandler(tornado.web.RequestHandler):

    @staticmethod
    def re_body(log_record_arr, lines, br, word_filter):
        if word_filter is not None:
            log_record_arr = [l for l in log_record_arr if l.find(word_filter) != -1]
        log_record_arr = log_record_arr[-1 * int(lines):]
        return ("<br>" * int(br)).join(log_record_arr)

    def data_received(self, chunk: bytes) -> Optional[Awaitable[None]]:
        pass

    def get(self):
        log_filename =  self.get_argument("f", "gateway-server.log")
        path = self.get_argument("path", "resource")
        lines = self.get_argument("lines", "50")
        br = self.get_argument("br", "1")
        word_filter = self.get_argument("word_filter", None)
        with open(f"./{path}/{log_filename}", "r") as f:
            log_record_arr = f.readlines()
            log_record = self.re_body(log_record_arr, lines, br, word_filter)
        self.write(f"<html><title>Logs Viewer</title><h1>Logs</h1><p>path={path}</p><p>f={log_filename}</p><p>lines={lines}</p><p>br={br}</p><p>word_filter={word_filter}</p><br><br>{log_record}</html>")
